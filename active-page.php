<?php
$link_current = $_SERVER['SCRIPT_NAME'];
$link_current = basename($link_current);

?>
<div class="menubar">
    <div class="menu">
        <a <?php echo ($link_current == "index.php") ? "class=\"select\"":""; ?> href="index.php">Home</a>
    </div>
    <div class="menu">
        <a  <?php echo ($link_current == "live-casino.php") ? "class=\"select\"":""; ?>  href="live-casino.php"  >Live Casino</a>
    </div>
    <div class="menu">
        <a  <?php echo ($link_current == "sportsbook.php" || $link_current == "support.php" ) ? "class=\"select\"":""; ?>  href="sportsbook.php" >Sportsbook</a>
    </div>
    <div class="menu">
        <a   <?php echo ($link_current == "lottery.php") ? "class=\"select\"":""; ?> href="lottery.php" >Lottery</a>
    </div>
    <div class="menu">
        <a   <?php echo ($link_current == "egames.php") ? "class=\"select\"":""; ?> href="egames.php">Egames</a>
    </div>
    <div class="menu" >
        <a   <?php echo ($link_current == "p2p-games.php") ? "class=\"select\"":""; ?> href="p2p-games.php">P2P Games</a>
    </div>
    <div class="menu" >
        <a   <?php echo ($link_current == "promotion.php") ? "class=\"select\"":""; ?> href="promotion.php" style="border-right: none;">Promotion</a>
    </div>

</div>
