<div id="footer">
    <div class="footertop">
        <div class="footertopcontent">
            <img class="diamond" src="images/diamond.png">
            <div class="bao-btt">
                <div class="title-bt">Banking Partner</div>
                <img class="img-bank" src="images/bank/bank.png" alt="">
            </div>
            <div class="socialmedia">
                <ul>
                    <li><a href="#"><img src="images/f.png" /></a></li>
                    <li><a href="#"><img src="images/t.png" /></a></li>
                </ul>
            </div>
            <div class="footerbox1">
                <ul>
                    <li><a href="#">About Us</a></li>
                    <li><a href="#">Privacy Policy</a></li>
                    <li><a href="#">Responsible Gambling</a></li>
                    <li><a href="#">Terms and Conditions</a></li>
                    <li><a href="#">Sitemap</a></li>
                    <li><a href="#">Affiliate Program</a></li>
                </ul>
            </div>
            <div class="footerbox2">
                <h3>Safety of your account</h3>
                <img class="security" src="images/safesecure.jpg"/>
                <ul>
                    <li><a href="#">Fully certified casino</a></li>
                    <li><a href="#">Strictly regulated fair games</a></li>
                    <li><a href="#">Safe monetary deposits</a></li>
                    <li><a href="#">Award winning casino</a></li>

                </ul>
            </div>


        </div>
    </div>

    <div class="footerbottom">
        <div class="footerbottomcontent">

            <div class="footerbottomleft">
                <img src="images/partners.png" style="width:800px;" alt=""><br/><br/>
            </div>

        </div>
    </div>

</div>


</div>

<!-- FlexSlider -->
<script defer src="js/jquery.flexslider.js"></script>

<script type="text/javascript">

    $(window).load(function(){
        $('.flexslider').flexslider({
            animation: "slide",
            start: function(slider){
                $('body').removeClass('loading');
            }
        });
    });
</script>

<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js'></script>
<script src="js/index.js"></script>

</body>

<!-- Mirrored from demo.krititech.in/power/paymenthistory.htm by HTTrack Website Copier/3.x [XR&CO'2013], Sun, 11 Jun 2017 05:50:52 GMT -->
</html>