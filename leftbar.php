<?php
$active = "";
$link_current = $_SERVER['SCRIPT_NAME'];
$link_current = basename($link_current);
?>
<div id="leftbar">
    <div class="leftbox">
        <div class="title  <?php echo ($link_current == "profile.php") ? "active":""; ?>"><a href="profile.php">My Details</a></div>
        <div class="title <?php echo ($link_current == "mybet.php") ? "active":""; ?>"><a href="mybet.php">My Bets</a></div>
        <div class="title <?php echo ($link_current == "bettinghistory.php") ? "active":""; ?>"><a href="bettinghistory.php">Betting History</a></div>
        <div class="title <?php echo ($link_current == "deposit.php") ? "active":""; ?>"><a href="deposit.php">Deposit</a></div>
        <div class="title <?php echo ($link_current == "withdraw.php") ? "active":""; ?>"><a href="withdraw.php">Withdraw</a></div>
        <div class="title <?php echo ($link_current == "transfer.php") ? "active":""; ?>"><a href="transfer.php">Transaction</a></div>
        <div class="title <?php echo ($link_current == "transferhistory.php") ? "active":""; ?>"><a href="transferhistory.php">Transaction History</a></div>
        <div class="title <?php echo ($link_current == "paymenthistory.php") ? "active":""; ?>"><a href="paymenthistory.php">Payment History</a></div>
        <div class="title <?php echo ($link_current == "index.php") ? "active":""; ?>"><a href="index.php">Logout</a></div>

    </div>
</div>