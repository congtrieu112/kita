<?php require "header-detail.php"; ?>
        
        <div id="maincontainer" style="min-height: 600px; background: none;">
            <div class="topsection">


                <?php require "leftbar.php";?>
				
					<div id="rightbar">
						<div class="righttitle">Payment History</div>
						
						<div style="width: 100%; height: auto; float: left;  padding-bottom: 10px;">
							
							<table style="width: 100%;">
								<tr>
									<td><label>History Filter</label><br/>
									<select class="textbox1" style="width:215px; height: 35px;">
										<option>Today</option>
										<option>Yesterday</option>
										<option>Last 7 days</option>
										<option>Last 30 days</option>
										<option>Date Range</option>
									</select>
									</td>
									<td><label>From</label><br/><input type="text" class="textbox1" placeholder="yyyy/mm/dd" style="width:215px;"/></td>
									<td><label>To</label><br/><input type="text" class="textbox1" placeholder="yyyy/mm/dd" style="width:215px;"/></td>
									<td><br/><input type="submit" class="button" value="Submit" style="padding: 6px; height: 38px; margin-top: -5px;"/></td>
								</tr>
								
								
						</table>
							
							<table class="table1">
								<tr>
									<th>Time Date</th>
									<th>Transaction</th>
									<th>Operation</th>
									<th>Game</th>
									<th>Amount</th>
									<th>Status</th>
									
								</tr>
								<tr class="odd">
									<td>03:18:03 +05:30 09th Jun 2017 </td>
									<td></td>
									<td>Transfer To Game</td>
									<td>SpadeGaming</td>
									<td>-$20</td>
									<td>Reject</td>
									
									
								</tr>
								<tr class="even">
									<td>03:18:03 +05:30 09th Jun 2017 </td>
									<td></td>
									<td>Transfer To Game</td>
									<td>SpadeGaming</td>
									<td>-$20</td>
									<td>Approve</td>
									
								</tr>
								<tr class="odd">
									<td>03:18:03 +05:30 09th Jun 2017 </td>
									<td></td>
									<td>Transfer To Game</td>
									<td>SpadeGaming</td>
									<td>-$20</td>
									<td>Reject</td>
									
								</tr>
								<tr class="even">
									<td>03:18:03 +05:30 09th Jun 2017 </td>
									<td></td>
									<td>Transfer To Game</td>
									<td>SpadeGaming</td>
									<td>-$20</td>
									<td>Success</td>
									
								</tr>
								<tr class="odd">
									<td>03:18:03 +05:30 09th Jun 2017 </td>
									<td></td>
									<td>Transfer To Game</td>
									<td>SpadeGaming</td>
									<td>-$20</td>
									<td>Approve</td>
									
								</tr>
								<tr class="even">
									<td>03:18:03 +05:30 09th Jun 2017 </td>
									<td></td>
									<td>Transfer To Game</td>
									<td>SpadeGaming</td>
									<td>-$20</td>
									<td>Reject</td>
									
								</tr>
								
							</table>
						</div>
						
						
						

						
					</div>
                <div id="leftBarMenu">
                    <div class="boxBorder">
                        <table class="table1">
                            <tbody><tr>

                                <th>Game</th>
                                <th>Game Balance</th>


                            </tr>
                            <tr class="odd">
                                <td>HappyNalo</td>
                                <td>107.98</td>


                            </tr>
                            <tr class="even">
                                <td>HappyNalo</td>
                                <td>107.98</td>


                            </tr>
                            <tr class="odd">
                                <td>HappyNalo</td>
                                <td>107.98</td>


                            </tr>
                            <tr class="even">
                                <td>HappyNalo</td>
                                <td>107.98</td>


                            </tr>
                            <tr class="odd">
                                <td>HappyNalo</td>
                                <td>107.98</td>


                            </tr>
                            <tr class="even">
                                <td>HappyNalo</td>
                                <td>107.98</td>


                            </tr>

                            </tbody></table>
                    </div></div>
				
				
				</div>
            
            
            
            
        </div>
        
        
       <?php require "footer-detail.php"; ?>