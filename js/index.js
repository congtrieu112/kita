$(function() {
  
  // Vars
  var modBtn  = $('#modBtn'),
      modal   = $('#modal'),
      close   = modal.find('.close'),
      modContent = modal.find('.modal-content');
    var modBtnLg  = $('.modBtnLg'),
        modalg   = $('#modalg'),
        closel   = modalg.find('.closed'),
        modContentl = modalg.find('.modal-contentl');


        var join = $('.join-f');
  
  // open modal when click on open modal button 
  modBtn.on('click', function() {
    modal.css('display', 'block');
    modContent.removeClass('modal-animated-out').addClass('modal-animated-in');
  });

    modBtnLg.on('click', function() {
        modalg.css('display', 'block');
        modContentl.removeClass('modal-animated-out').addClass('modal-animated-in');
    });
    join.on('click', function() {
      closel.trigger('click');
    modal.css('display', 'block');
    modContent.removeClass('modal-animated-out').addClass('modal-animated-in');
  })
  
  // close modal when click on close button or somewhere out the modal content 
  $(document).on('click', function(e) {
    var target = $(e.target);
    if(target.is(modal) || target.is(close)) {
      modContent.removeClass('modal-animated-in').addClass('modal-animated-out').delay(300).queue(function(next) {
        modal.css('display', 'none');
        next();
      });
    }

      if( target.is(closel) || target.is(modalg)) {
          modContentl.removeClass('modal-animated-in').addClass('modal-animated-out').delay(300).queue(function(next) {
              modalg.css('display', 'none');
              next();
          });
      }
  });

  $("#siderbar").find("p").on("click",function (argument) {
    // body...
    
      

      if($("#siderbar").hasClass("siderbar_close")){
        $("#siderbar").removeClass("siderbar_close");
      }else{
        $("#siderbar").addClass("siderbar_close");
      }
  })
  
});